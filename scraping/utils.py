import time
import httpx
import asyncio
import rich
import copy 

def _get_response_content(response):
    return response.content

def _get_response_json(response):
    return response.json()

def print_message(msg, logger=None, log_level='INFO'):
    if logger is None:
        rich.print(msg)
        return
    
    log_fns = {
        'INFO': logger.info,
        'WARNING': logger.warning,
        'ERROR': logger.error,
        'DEBUG': logger.debug        
    }
    log_fn = log_fns[log_level.upper()]
    log_fn(msg, extra={"markup": True})
    
async def scrape(
    main_url, 
    param_key=None,
    param_vals=[], 
    param_list=[], 
    const_params={}, 
    process_fn=None, 
    raw_urls=[],
    retries=5, 
    timeout=10, 
    description='',
    transient_progress=False,
    logger=None,
    client_limit_kwargs=dict()
):
    _start = time.time()
    responses = []
     
    if param_key is not None:
        assert len(param_vals) > 0, \
            '`param_vals` needs to be a non-empty list if `param_key` is given'
        assert len(param_list) == 0, \
            '`param_list` is not a valid input if `param_key` is given'
        param_list = [{param_key: val} for val in param_vals]
    else:
        assert len(param_vals) == 0, \
            '`param_vals` is not a valid input list if `param_key` is NOT given (`None`)'
        if len(param_list) > 0:
            assert all([type(x) is dict for x in param_list]), \
                '`param_list` needs to be a list of `dict` if `param_key` is NOT given'
        else:
            assert len(raw_urls) > 0, \
                '`raw_urls` cannot be an empty list if `param_list` or (`param_key`, `param_vals`) is not valid'
            
    total = len(param_list) + len(raw_urls)
    
    transport = httpx.AsyncHTTPTransport(retries=retries)
    limits = httpx.Limits(**client_limit_kwargs)
    
    if len(description) > 0:
        msg = f"Starting: [bold]{description}[/bold] ..."
        print_message(msg, logger=logger, log_level='INFO')
        
    if not callable(process_fn) or process_fn is None:
        process_fn = _get_response_content
        
    with rich.progress.Progress(
            rich.progress.TextColumn(
                "[yellow]{task.description}[/yellow]",
                table_column=rich.table.Column(ratio=1)
            ),
            "[red][progress.percentage]{task.percentage:>3.0f}%[/red]",
            rich.progress.BarColumn(
                bar_width=None,
                table_column=rich.table.Column(ratio=2)
            ),
            rich.progress.TextColumn(
                '[yellow][{task.completed}/{task.total}][/yellow]', 
                table_column=rich.table.Column(ratio=1)
            ),
            rich.progress.SpinnerColumn(
                table_column=rich.table.Column(ratio=1)
            ),
            transient=transient_progress
        
    ) as progress:
        task = progress.add_task("Requesting ...", total=total)

        async with httpx.AsyncClient(transport=transport, limits=limits) as client:
            tasks = [
                client.get(
                    main_url, 
                    params = {
                        **const_params, 
                        **_params
                    }, 
                    timeout = timeout
                ) 
                for _params in param_list
            ] + [
                client.get(str(_raw_url))
                for _raw_url in raw_urls
            ]

            for response_future in asyncio.as_completed(tasks):
                response = await response_future
                response_data = dict(
                    url = response.url, 
                    data = None,
                    success = False
                )

                if response.status_code == httpx.codes.OK:
                    response_data['data'] = process_fn(response)
                    response_data['success'] = True
                else:
                    msg = f'[red][bold]ERROR[/bold] at requesting [italic]{response.url}[/italic][/]'
                    print_message(msg, logger=logger, log_level='ERROR')
 
                responses.append(response_data)
                progress.update(task, advance=1)
                
    msg = f"Finished in: {time.time() - _start:.2f} seconds"
    print_message(msg, logger=logger, log_level='INFO')
    
    return responses

def filter_responses(responses, field='data', success=True):
    return [x[field] for x in responses if x['success'] == success]


def rerun_failed_urls(
    responses,
    params, 
    sleep_time=20,
    sleep_time_per_failed=1,
    logger=None,
    copy_params=True,  
):
    if copy_params:
        params = copy.deepcopy(params)
        
    failed = filter_responses(responses, field='url', success=False)
        
    rerun_responses = []
    if (_num_old_failed:=len(failed)) > 0:
        _sleep_time = min(
            int(sleep_time_per_failed * _num_old_failed), 
            sleep_time
        )
            
        print_message(
            msg = f'Failures ({_num_old_failed}) encountered. Take a break for {_sleep_time} seconds.',
            logger = logger, 
            log_level = 'WARNING'
        )
        
        time.sleep(sleep_time)
        
        params['param_key'] = None
        params['param_vals'] = []        
        params['param_list'] = [] 
        params['raw_urls'] = failed 
        
        rerun_responses = asyncio.run(scrape(**params))
        failed = filter_responses(rerun_responses, field='url', success=False)
        print_message(
            msg = f'Fixed {_num_old_failed-(_num_new_failed:=len(failed))} from {_num_old_failed}. '\
                    f'Remaining failures: {_num_new_failed}. Will NOT fix. Sorry!',
            logger = logger,
            log_level = 'WARNING'
        )
        
    responses = responses + rerun_responses
    return responses, failed
import re
import string
import pickle
import numpy as np 

from datetime import datetime
from bs4 import BeautifulSoup
import asyncio
import utils 

import logging
from rich.logging import RichHandler

def _get_subreddit_names(response):
    subred_list = []
    soup = BeautifulSoup(response.content, 'html.parser')

    for link in soup.find_all('a', class_='search_subreddit', href=re.compile(r'^/r/')):
        name = link.get('href').replace('/r/','').replace('/','').strip().lower()
        subred_list.append(name)
    return subred_list

def finalize_subreddit_list(responses, add, remove):
    add = [x.lower() for x in add]
    remove = [x.lower() for x in remove]
    subreddit_list = np.concatenate(utils.filter_responses(responses, field='data') + [add])
    subreddit_list = list(set(subreddit_list) - set(remove))
    return subreddit_list

if __name__ == '__main__':

    # Define paths
    data_path = 'data/raw/theletters-stats.pkl'
    log_path = f"logs/scrape_stats_{datetime.now().strftime('%Y-%m-%d_%H-%M-%S')}.log"
    
    # Define logging
    logging.basicConfig(
        level="INFO", 
        format="%(message)s",
        datefmt="[%X]", 
        handlers=[
            RichHandler(),
            logging.FileHandler(log_path)
        ]
    )
    
    logger = logging.getLogger("rich")

    # Define parameters
    ascii_subreddits = [f'theletter{x}' for x in string.ascii_lowercase]
    
    list_params = dict(
        description = 'Get subreddit list',
        main_url = 'https://libreddit.nl/r/all/search',
        const_params = {'sort': 'relevance', 't': 'all', 'type': 'sr_user'},
        param_key = 'q',
        param_vals = ['theletter'] + ascii_subreddits,
        process_fn = _get_subreddit_names,
        logger = logger
    )
    not_letter = ['TheLetterForTheKing', 'suddenlycaralho'] # known to be not part of the letter wars
    additional_subreddits = ['TheForgottenLetters', 'TheOtherLetters'] # additional ones
    
    srs_params = dict(
        description = 'Obtain subreddit stats',
        main_url = 'https://subredditstats.com/api/subreddit',
        param_key = 'name',
        process_fn = utils._get_response_json,
        logger = logger
    )
    
    # 1. Get subreddit candidate list
    list_responses = asyncio.run(utils.scrape(**list_params))
    subreddit_list = finalize_subreddit_list(
        list_responses, 
        add = ascii_subreddits + additional_subreddits,
        remove = not_letter
    )
    
    # 2. Query subredditstats from list
    srs_params['param_vals'] = subreddit_list    
    stats_responses = asyncio.run(utils.scrape(**srs_params))
    
    # 3. Rerun failed URLs from subredditstats
    stats_responses, _ = utils.rerun_failed_urls(
        responses = stats_responses,
        params = srs_params,
        sleep_time = 20,
        logger = logger,
        copy_params = True
    )
    
    # 4. Write to file
    with open(data_path, 'wb') as f:
        pickle.dump(stats_responses, f)
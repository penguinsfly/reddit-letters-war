import time
from datetime import datetime 
import pickle

import numpy as np
import pandas as pd

import asyncio

import rich
from tqdm import tqdm
import logging
from rich.logging import RichHandler

import utils


def construct_daterange_params(utc_starts, utc_ends):            
    date_ranges = [
        {
            'after': int(_start),
            'before': int(_end)
        }
        for _start, _end in zip(utc_starts, utc_ends)
        if _start < _end
    ] 
    return date_ranges

def replace_ranges(utc_starts, utc_ends, new_starts):
    old_starts, old_ends = utc_starts, utc_ends
    utc_starts, utc_ends = [], [] 

    for _new in new_starts:
        if _new is None:
            continue
            
        _idx = np.where((_new - old_starts) * (_new - old_ends) < 0)[0]
        assert len(_idx) < 2 
        if len(_idx) == 0:
            continue
        _idx = _idx[0]
        
        new_start = _new
        new_end = old_ends[_idx]

        utc_starts.append(new_start)
        utc_ends.append(new_end)
        
    return np.array(utc_starts), np.array(utc_ends)

def preprocess_response(response):
    select_fields = ['subreddit', 'author', 'created_utc', 'link_id', 'score', 'body']
    data = response.json().get('data', [])
    if len(data) == 0:
        return None
     
    df = pd.DataFrame(data).filter(select_fields)
    return df

def get_comments_pushshift(
    subreddit,
    num_segments=20, 
    size=500,                            
    loop_sleep_time=10, 
    retries=5, 
    timeout=60,
    transient_progress=True,
    logger=None,
    client_limit_kwargs=dict()
):

    params = dict(
        main_url = 'https://api.pushshift.io/reddit/search/comment',
        const_params = {
            'subreddit': subreddit,
            'size': size
        },
        process_fn = preprocess_response,
        retries = retries,
        timeout = timeout,
        transient_progress = transient_progress,
        logger = logger,
        client_limit_kwargs = client_limit_kwargs
    )

    segments = np.floor(np.linspace(start_at, end_at, num_segments+1)).astype('int')
    segment_starts, segment_ends = segments[:-1]+1, segments[1:]

    aggregrated = []
    failed_urls = []

    _start_time_global = time.time()
    _loop_iter = 1

    while num_segments > 0:
        params['raw_urls'] = []
        params['description'] = f'[SUBREDDIT={subreddit}, ITER={_loop_iter}, NUM_SEGMENTS={num_segments}]'
        params['param_list'] = construct_daterange_params(segment_starts, segment_ends)

        responses = asyncio.run(utils.scrape(**params))
        
        responses, failed = utils.rerun_failed_urls(
            responses = responses,
            params = params,
            sleep_time = loop_sleep_time,
            logger = logger,
            copy_params = True
        )
        
        success_data = [x for x in utils.filter_responses(responses, field='data') 
                        if x is not None]

        failed_urls.extend(failed)
        aggregrated.extend(success_data)

        new_starts = [_df.created_utc.max() for _df in success_data]    
        segment_starts, segment_ends = replace_ranges(segment_starts, segment_ends, new_starts)

        if (num_segments:=len(segment_starts)) == 0:
            break

        time.sleep(loop_sleep_time)
        
    utils.print_message(
        msg = f"TOTAL TIME: {(time.time() - _start_time_global)/60:.2f} minutes",
        logger = logger, 
        log_level = 'INFO'
    )
    
    return dict(
        subreddit = subreddit,
        comments = aggregrated,
        failed_urls = failed_urls
    )


if __name__ == '__main__':

    # Define paths
    stats_path = 'data/raw/theletters-stats.pkl'
    data_path = 'data/raw/theletters-comments.pkl'
    log_path = f"logs/scrape_comments_{datetime.now().strftime('%Y-%m-%d_%H-%M-%S')}.log"
    
    # Define logging
    logging.basicConfig(
        level="INFO", 
        format="%(message)s",
        datefmt="[%X]", 
        handlers=[
            RichHandler(),
            logging.FileHandler(log_path, mode='a')
        ]
    )
    
    logger = logging.getLogger("rich")
    
    # Some parameters
    client_limit_kwargs = dict(
        max_keepalive_connections=10, 
        max_connections=50
    )
    
    # Read from the stats file to get metadata
    not_letters = ['suddenlycaralho'] # additional removal 

    with open(stats_path, 'rb') as f:
        raw_data = pickle.load(f)

    df_metadata = pd.DataFrame([x['data'] for x in raw_data if x['success']])
    df_metadata = df_metadata.query('name.str.lower() not in @not_letters').reset_index()

    subreddit_lists = list(df_metadata.name)

    min_created_utc = pd.to_datetime(df_metadata['creationTime'], unit='ms', utc=True).min()
    start_at = int((min_created_utc - pd.Timedelta(30, unit='d')).timestamp()) # shift 30d back
    end_at = int(datetime.now().timestamp())

    # Obtain data
    results = [] 
    for subreddit in tqdm(subreddit_lists, desc='Comment scraping'):
        results.append(get_comments_pushshift(
            subreddit=subreddit,
            logger=logger,
            client_limit_kwargs=client_limit_kwargs
        ))
        
        # Write to file
        with open(data_path, 'wb') as f:
            pickle.dump(results, f)
SHELL := /bin/bash

help: ## Show this help
	@egrep -h '\s##\s' $(MAKEFILE_LIST) | sort | awk 'BEGIN {FS = ":.*?## "}; {printf "\033[36m%-20s\033[0m %s\n", $$1, $$2}'

scrape-stats: ## Scrape subreddit stats
	python scraping/obtain_stats.py

scrape-comments: ## Scrape subreddit comments
	python scraping/obtain_comments.py
    
scrape: ## Scrape all
	make scrape-stats
	make scrape-comments
